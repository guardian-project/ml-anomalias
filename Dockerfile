FROM opencpu/ubuntu-20.04


RUN mkdir /home/analysis
RUN chmod 777 /home/analysis

COPY ./*.tar.gz /home/analysis

RUN R -e "install.packages(c('devtools', 'lubridate', 'httr', 'imputeTS', 'tsoutliers', 'prophet', 'magrittr'))"
RUN R -e "devtools::install_github('base/sensorsUtils')"
RUN R -e "install.packages('/home/analysis/guardianML_0.1.0_R_x86_64-pc-linux-gnu.tar.gz', repos = NULL, type='source')"


